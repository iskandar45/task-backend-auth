﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JwtAuthorization.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public TokenController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        [Route("GenerateToken")]
        public ActionResult GenerateToken()
        {
            string jwtToken = "";
            jwtToken = Logics.TokenLogics.GenerateToken(_configuration);
            return Ok(jwtToken);
        }

        [HttpGet]
        [Route("ValidateToken")]
        [Authorize]
        public ActionResult ValidateToken()
        {
            return Ok("Congrats, your token is right!");
        }
    }
}
